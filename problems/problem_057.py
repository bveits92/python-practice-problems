# Write a function that meets these requirements.
#
# Name:       sum_fraction_sequence
# Parameters: a number
# Returns:    the sum of the fractions of the
#             form 1/2+2/3+3/4+...+number/number+1
#
# Examples:
#     * input:   1
#       returns: 1/2
#     * input:   2
#       returns: 1/2 + 2/3
#     * input:   3
#       returns: 1/2 + 2/3 + 3/4

# numerator, denominator

def sum_fraction_sequence(number):
    result = 0
    numerator = 1
    denominator = numerator

    while numerator <= number:
        result += numerator / denominator

        numerator += 1
        denominator += 1

    return result
