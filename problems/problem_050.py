# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(input):
    list1 = []
    list2 = []
    if len(input) % 2 != 0:
        list1.append(input[:len(input) // 2 + 1])
        list2.append(input[len(input) // 2 - 1:])
    else:
        list1.append(input[:len(input) // 2])
        list2.append(input[len(input) // 2:])

    return list1 + list2

check = [1, 2], [3, 4]

print(halve_the_list(check))
