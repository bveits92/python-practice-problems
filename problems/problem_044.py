# Complete the translate function which accepts two
# parameters, a list of keys and a dictionary. It returns a
# new list that contains the values of the corresponding
# keys in the dictionary. If the key does not exist, then
# the list should contain a None for that key.
#
# Examples:
#   * keys:       ["name", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     ["Noor", 29]
#   * keys:       ["eye color", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [None, 29]
#   * keys:       ["age", "age", "age"]
#     dictionary: {"name": "Noor", "age": 29}
#     result:     [29, 29, 29]
#
# Remember that a dictionary has the ".get" method on it.

def translate(key_list, dictionary):
    # Create a result list
    result = []
    # Iterate over the dict
    for key_l in key_list:
    # Iterate inside the loop with iteration in keys
        if key_l in dictionary:
    # Append to the result list
            result.append(dictionary[key_l])
        else:
            result.append(None)
    # Return the result list

    return result

list1 = ["eye color", "age"]
dict = {"name": "Noor", "age": 29}

print(translate(list1, dict))
