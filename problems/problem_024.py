# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    sum = 0

    for number in values:
        sum += number

    if len(values) == 0:
        return None
    else:
        return sum / len(values)
