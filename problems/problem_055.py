# Write a function that meets these requirements.
#
# Name:       simple_roman
# Parameters: one parameter that has a value from 1
#             to 10, inclusive
# Returns:    the Roman numeral equivalent of the
#             parameter value
#
# All examples
#     * input: 1
#       returns: "I"
#     * input: 2
#       returns: "II"
#     * input: 3
#       returns: "III"
#     * input: 4
#       returns: "IV"
#     * input: 5
#       returns: "V"
#     * input: 6
#       returns: "VI"
#     * input: 7
#       returns: "VII"
#     * input: 8
#       returns: "VIII"
#     * input: 9
#       returns: "IX"
#     * input: 10
#       returns:  "X"

def simple_roman(number):
    result = []
    if number >= 1 and number <= 10:
        if number == 1:
            return "I"
        elif number == 2:
            return "II"
        elif number == 3:
            return "III"
        elif number == 4:
            return "IV"
        elif number == 5:
            return "V"
        elif number == 6:
            return "VI"
        elif number == 7:
            return "VII"
        elif number == 8:
            return "VIII"
        elif number == 9:
            return "IX"
        else:
            return "X"
